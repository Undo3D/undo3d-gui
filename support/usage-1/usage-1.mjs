import '../lib/console.mjs'
import { CanvasGui, DomGui, TtyGui, ThreeGui } from '../../src/all.mjs'
import { Unit } from '../lib/sodium/unit.mjs'
import { StreamSink } from '../lib/sodium/stream-sink.mjs'
const
    frpLib = { Unit, StreamSink }
  , assetPath = '../../src/asset/'

if ('object' !== typeof document && 'object' !== typeof global)
    console.error('Unexpected runtime environment!')

try {

    const guis =
        'object' === typeof global ? [
            new TtyGui({ w:300, h:200, d:100, frpLib })
        ]
      : 'object' === typeof document ? [
               new DomGui(   { selector:'#dom-gui',    w:300, h:200, d:100, frpLib })
             , new TtyGui(   { selector:'#tty-gui',    w:300, h:200, d:100, frpLib })
             , new CanvasGui({ selector:'#canvas-gui', w:300, h:200, d:100, frpLib })
             , new ThreeGui( { selector:'#three-gui',  w:300, h:200, d:100, frpLib, assetPath })
        ]
      : []

    guis.forEach( gui => {
        const

            //// Create the GUI elements.
            clickHereButton = gui.addButton({ text:'Click Here'
              , x:60, y:20, z:3, w:170 })
          , mouseDownUpText = gui.addText({ text:'...'
              , x:60, y:80, z:3, w:170 })
          , mouseOverOutText = gui.addText({ text:'...'
              , x:60, y:120, z:3, w:170 })
          , mouseMoveText = gui.addText({ text:'...'
              , x:60, y:160, z:3, w:170 })

            //// Hook up listeners to the ‘Click Here’ button.
          , mousedownStream = clickHereButton
               .on('mousedown touchstart')
               .map( u => 'Mousedown' )
          , mouseupStream = clickHereButton
               .on('mouseup touchend')
               .map( u => 'Mouseup' )
          , mouseoverStream = clickHereButton
               .on('mouseover')
               .map( u => 'Mouseover' )
          , mouseoutStream = clickHereButton
               .on('mouseout')
               .map( u => 'Mouseout' )
          , mousemoveStream = clickHereButton
               .on('mousemove')
               .map( u => 'Mousemove' )

        //// Merge the streams, and send them to the output boxes.
        mousedownStream
           .merge(mouseupStream)
           .listen( text => mouseDownUpText.setText({ text }) )
        mouseoverStream
           .merge(mouseoutStream)
           .listen( text => mouseOverOutText.setText({ text }) )
        mousemoveStream
           .listen( text => mouseMoveText.setText({ text }) )
    })

    console.log('Rendered ' + guis.length + ' GUI' + (1 === guis.length ? '' : 's'))

} catch (err) {
    console.error(err)
}
