import '../lib/console.mjs'
import { CanvasGui, DomGui, TtyGui, ThreeGui } from '../../src/all.mjs'
import { Unit } from '../lib/sodium/unit.mjs'
import { StreamSink } from '../lib/sodium/stream-sink.mjs'
const
    frpLib = { Unit, StreamSink }
  , assetPath = '../../src/asset/'

if ('object' !== typeof document && 'object' !== typeof global)
    console.error('Unexpected runtime environment!')

try {

    const guis =
        'object' === typeof global ? [
            new TtyGui({ w:300, h:200, d:100, frpLib })
        ]
      : 'object' === typeof document ? [
               new DomGui(   { selector:'#dom-gui',    w:300, h:200, d:100, frpLib })
             , new TtyGui(   { selector:'#tty-gui',    w:300, h:200, d:100, frpLib })
             , new CanvasGui({ selector:'#canvas-gui', w:300, h:200, d:100, frpLib })
             , new ThreeGui( { selector:'#three-gui',  w:300, h:200, d:100, frpLib, assetPath })
        ]
      : []

    guis.forEach( gui => {
        const

            //// Create the GUI elements.
            inputText = gui.addInputText({ placeholder:'Click and type'
              , x:40, y:40, z:3, w:220 })
          , btn = gui.addButton({ text:'OK'
              , x:40, y:100, z:3, w:110 })
          , in2 = gui.addInputText({ placeholder:'Hi'
              , x:180, y:100, z:3, w:80 })
          , focusStateText = gui.addText({ text:'...'
              , x:20, y:160, z:3, w:250 })

            //// Hook up listeners to the ‘Click and type' input.
          , inputTextStream = inputText
               .on('mousedown')
               .map( u => 'mousedown: text-input' )
          , buttonStream = btn
               .on('mousedown')
               .map( u => 'mousedown: ‘OK’ button' )

        //// Send the stream to the output box.
        inputTextStream
           .merge(buttonStream)
           .listen( text => focusStateText.setText({ text }) )
    })

    console.log('Rendered ' + guis.length + ' GUI' + (1 === guis.length ? '' : 's'))

} catch (err) {
    console.error(err)
}
