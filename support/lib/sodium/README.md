# Process for converting sodium-typescript to .mjs files:

1. Visit https://github.com/SodiumFRP/sodium-typescript
2. Download the zip file and unzip it
3. `$ npm install -g typescript`
4. `$ cd <path/to/unzipped/repo>/sodium-typescript-master/src/lib/sodium`
5. `$ tsc *.ts --target ES6 # ignore ‘Cannot find module’ errors`
8. `$ cd <path/to/this/project>`
6. `$ mkdir lib/sodiumjs`
4. `$ cd -`
7. `$ mv *.js <path/to/this/project>/lib/sodiumjs`
8. `$ cd -`
9. `$ node support/convert-sodium-ts-to-mjs.js`
10. In stream.mjs, comment-out line 9, `import { LazyCell } ...` (circular dep)
11. In stream.mjs, comment-out lines 321-323, `holdLazy(initValue) { ... }`
