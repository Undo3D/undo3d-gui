import { StreamWithSend } from './stream.mjs'
import { Vertex, Source } from './vertex.mjs'
import { Transaction } from './transaction.mjs'
export class IOAction {
    /*!
     * Convert a function that performs asynchronous I/O taking input A
     * and returning a value of type B into an I/O action of type
     * (sa : Stream<A>) => Stream<B>
     */
    static fromAsync(performIO) {
        return (sa) => {
            const out = new StreamWithSend(null);
            out.setVertex__(new Vertex("map", 0, [
                new Source(sa.getVertex__(), () => {
                    return sa.listen_(out.getVertex__(), (a) => {
                        performIO(a, (b) => {
                            Transaction.run(() => {
                                out.send_(b);
                            });
                        });
                    }, false);
                })
            ]));
            return out;
        };
    }
}


//// support/convert-sodium-ts-examples-to-mjs.js converted to .mjs.
if ('object' === typeof window && 'function' === typeof CustomEvent)
    window.dispatchEvent( new CustomEvent('lib-module-loaded', {
        detail: 'lib/sodiumjs/i-o-action.mjs' }) )