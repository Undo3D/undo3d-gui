import { Lambda1_deps, Lambda1_toFunction, Lambda2_deps, Lambda2_toFunction, Lambda3_deps, Lambda3_toFunction, Lambda4_deps, Lambda4_toFunction, Lambda5_deps, Lambda5_toFunction, Lambda6_deps, Lambda6_toFunction, toSources } from './lambda.mjs'
import { Source, Vertex } from './vertex.mjs'
import { Transaction } from './transaction.mjs'
import { CoalesceHandler } from './coalesce-handler.mjs'
import { Cell } from './cell.mjs'
//import { StreamLoop } from "./StreamLoop";
import { Listener } from './listener.mjs'
import { Lazy } from './lazy.mjs'
//// import { LazyCell } from './lazy-cell.mjs' //20181205^RP removed circular dependency
import { LazyCell } from './lazy-cell.mjs'
import * as Z from '../sanctuary-type-classes/index.mjs';
export class Stream {
    constructor(vertex) {
        this.listeners = [];
        this.firings = [];
        this.vertex = vertex ? vertex : new Vertex("Stream", 0, []);
    }
    getVertex__() {
        return this.vertex;
    }
    /**
     * Transform the stream's event values according to the supplied function, so the returned
     * Stream's event values reflect the value of the function applied to the input
     * Stream's event values.
     * @param f Function to apply to convert the values. It may construct FRP logic or use
     *    {@link Cell#sample()} in which case it is equivalent to {@link Stream#snapshot(Cell)}ing the
     *    cell. Apart from this the function must be <em>referentially transparent</em>.
     */
    map(f) {
        const out = new StreamWithSend(null);
        const ff = Lambda1_toFunction(f);
        out.vertex = new Vertex("map", 0, [
            new Source(this.vertex, () => {
                return this.listen_(out.vertex, (a) => {
                    out.send_(ff(a));
                }, false);
            })
        ].concat(toSources(Lambda1_deps(f))));
        return out;
    }
    /**
     * Transform the stream's event values into the specified constant value.
     * @param b Constant value.
     */
    mapTo(b) {
        const out = new StreamWithSend(null);
        out.vertex = new Vertex("mapTo", 0, [
            new Source(this.vertex, () => {
                return this.listen_(out.vertex, (a) => {
                    out.send_(b);
                }, false);
            })
        ]);
        return out;
    }
    /**
     * Variant of {@link Stream#merge(Stream, Lambda2)} that merges two streams and will drop an event
     * in the simultaneous case.
     * <p>
     * In the case where two events are simultaneous (i.e. both
     * within the same transaction), the event from <em>this</em> will take precedence, and
     * the event from <em>s</em> will be dropped.
     * If you want to specify your own combining function, use {@link Stream#merge(Stream, Lambda2)}.
     * s1.orElse(s2) is equivalent to s1.merge(s2, (l, r) -&gt; l).
     * <p>
     * The name orElse() is used instead of merge() to make it really clear that care should
     * be taken, because events can be dropped.
     */
    orElse(s) {
        return this.merge(s, (left, right) => {
            return left;
        });
    }
    merge_(s) {
        const out = new StreamWithSend();
        const left = new Vertex("merge", 0, []);
        left.sources.push(new Source(this.vertex, () => {
            return this.listen_(left, (a) => {
                out.send_(a);
            }, false);
        }));
        out.vertex.sources = out.vertex.sources.concat([
            new Source(left, () => {
                left.register(out.vertex);
                return () => { left.deregister(out.vertex); };
            }),
            new Source(s.vertex, () => {
                return s.listen_(out.vertex, (a) => {
                    out.send_(a);
                }, false);
            })
        ]);
        return out;
    }
    coalesce__(f) {
        const out = new StreamWithSend();
        const coalescer = new CoalesceHandler(f, out);
        out.vertex.sources = out.vertex.sources.concat([
            new Source(this.vertex, () => {
                return this.listen_(out.vertex, (a) => {
                    coalescer.send_(a);
                }, false);
            })
        ]).concat(toSources(Lambda2_deps(f)));
        return out;
    }
    /**
     * Merge two streams of the same type into one, so that events on either input appear
     * on the returned stream.
     * <p>
     * If the events are simultaneous (that is, one event from this and one from <em>s</em>
     * occurring in the same transaction), combine them into one using the specified combining function
     * so that the returned stream is guaranteed only ever to have one event per transaction.
     * The event from <em>this</em> will appear at the left input of the combining function, and
     * the event from <em>s</em> will appear at the right.
     * @param f Function to combine the values. It may construct FRP logic or use
     *    {@link Cell#sample()}. Apart from this the function must be <em>referentially transparent</em>.
     */
    merge(s, f) {
        return Transaction.run(() => {
            return this.merge_(s).coalesce__(f);
        });
    }
    /**
     * Return a stream that only outputs events for which the predicate returns true.
     */
    filter(f) {
        const out = new StreamWithSend(null);
        const ff = Lambda1_toFunction(f);
        out.vertex = new Vertex("filter", 0, [
            new Source(this.vertex, () => {
                return this.listen_(out.vertex, (a) => {
                    if (ff(a))
                        out.send_(a);
                }, false);
            })
        ].concat(toSources(Lambda1_deps(f))));
        return out;
    }
    /**
     * Return a stream that only outputs events that have present
     * values, discarding null values.
     */
    filterNotNull() {
        const out = new StreamWithSend(null);
        out.vertex = new Vertex("filterNotNull", 0, [
            new Source(this.vertex, () => {
                return this.listen_(out.vertex, (a) => {
                    if (a !== null)
                        out.send_(a);
                }, false);
            })
        ]);
        return out;
    }
    /**
     * Return a stream that only outputs events from the input stream
     * when the specified cell's value is true.
     */
    gate(c) {
        return this.snapshot(c, (a, pred) => {
            return pred ? a : null;
        }).filterNotNull();
    }
    /**
     * Variant of {@link snapshot(Cell, Lambda2)} that captures the cell's value
     * at the time of the event firing, ignoring the stream's value.
     */
    snapshot1(c) {
        const out = new StreamWithSend(null);
        out.vertex = new Vertex("snapshot1", 0, [
            new Source(this.vertex, () => {
                return this.listen_(out.vertex, (a) => {
                    out.send_(c.sampleNoTrans__());
                }, false);
            }),
            new Source(c.getVertex__(), null)
        ]);
        return out;
    }
    /**
     * Return a stream whose events are the result of the combination using the specified
     * function of the input stream's event value and the value of the cell at that time.
     * <P>
     * There is an implicit delay: State updates caused by event firings being held with
     * {@link Stream#hold(Object)} don't become visible as the cell's current value until
     * the following transaction. To put this another way, {@link Stream#snapshot(Cell, Lambda2)}
     * always sees the value of a cell as it was before any state changes from the current
     * transaction.
     */
    snapshot(b, f_) {
        const out = new StreamWithSend(null);
        const ff = Lambda2_toFunction(f_);
        out.vertex = new Vertex("snapshot", 0, [
            new Source(this.vertex, () => {
                return this.listen_(out.vertex, (a) => {
                    out.send_(ff(a, b.sampleNoTrans__()));
                }, false);
            }),
            new Source(b.getVertex__(), null)
        ].concat(toSources(Lambda2_deps(f_))));
        return out;
    }
    /**
     * Return a stream whose events are the result of the combination using the specified
     * function of the input stream's event value and the value of the cells at that time.
     * <P>
     * There is an implicit delay: State updates caused by event firings being held with
     * {@link Stream#hold(Object)} don't become visible as the cell's current value until
     * the following transaction. To put this another way, snapshot()
     * always sees the value of a cell as it was before any state changes from the current
     * transaction.
     */
    snapshot3(b, c, f_) {
        const out = new StreamWithSend(null);
        const ff = Lambda3_toFunction(f_);
        out.vertex = new Vertex("snapshot", 0, [
            new Source(this.vertex, () => {
                return this.listen_(out.vertex, (a) => {
                    out.send_(ff(a, b.sampleNoTrans__(), c.sampleNoTrans__()));
                }, false);
            }),
            new Source(b.getVertex__(), null),
            new Source(c.getVertex__(), null)
        ].concat(toSources(Lambda3_deps(f_))));
        return out;
    }
    /**
     * Return a stream whose events are the result of the combination using the specified
     * function of the input stream's event value and the value of the cells at that time.
     * <P>
     * There is an implicit delay: State updates caused by event firings being held with
     * {@link Stream#hold(Object)} don't become visible as the cell's current value until
     * the following transaction. To put this another way, snapshot()
     * always sees the value of a cell as it was before any state changes from the current
     * transaction.
     */
    snapshot4(b, c, d, f_) {
        const out = new StreamWithSend(null);
        const ff = Lambda4_toFunction(f_);
        out.vertex = new Vertex("snapshot", 0, [
            new Source(this.vertex, () => {
                return this.listen_(out.vertex, (a) => {
                    out.send_(ff(a, b.sampleNoTrans__(), c.sampleNoTrans__(), d.sampleNoTrans__()));
                }, false);
            }),
            new Source(b.getVertex__(), null),
            new Source(c.getVertex__(), null),
            new Source(d.getVertex__(), null)
        ].concat(toSources(Lambda4_deps(f_))));
        return out;
    }
    /**
     * Return a stream whose events are the result of the combination using the specified
     * function of the input stream's event value and the value of the cells at that time.
     * <P>
     * There is an implicit delay: State updates caused by event firings being held with
     * {@link Stream#hold(Object)} don't become visible as the cell's current value until
     * the following transaction. To put this another way, snapshot()
     * always sees the value of a cell as it was before any state changes from the current
     * transaction.
     */
    snapshot5(b, c, d, e, f_) {
        const out = new StreamWithSend(null);
        const ff = Lambda5_toFunction(f_);
        out.vertex = new Vertex("snapshot", 0, [
            new Source(this.vertex, () => {
                return this.listen_(out.vertex, (a) => {
                    out.send_(ff(a, b.sampleNoTrans__(), c.sampleNoTrans__(), d.sampleNoTrans__(), e.sampleNoTrans__()));
                }, false);
            }),
            new Source(b.getVertex__(), null),
            new Source(c.getVertex__(), null),
            new Source(d.getVertex__(), null),
            new Source(e.getVertex__(), null)
        ].concat(toSources(Lambda5_deps(f_))));
        return out;
    }
    /**
     * Return a stream whose events are the result of the combination using the specified
     * function of the input stream's event value and the value of the cells at that time.
     * <P>
     * There is an implicit delay: State updates caused by event firings being held with
     * {@link Stream#hold(Object)} don't become visible as the cell's current value until
     * the following transaction. To put this another way, snapshot()
     * always sees the value of a cell as it was before any state changes from the current
     * transaction.
     */
    snapshot6(b, c, d, e, f, f_) {
        const out = new StreamWithSend(null);
        const ff = Lambda6_toFunction(f_);
        out.vertex = new Vertex("snapshot", 0, [
            new Source(this.vertex, () => {
                return this.listen_(out.vertex, (a) => {
                    out.send_(ff(a, b.sampleNoTrans__(), c.sampleNoTrans__(), d.sampleNoTrans__(), e.sampleNoTrans__(), f.sampleNoTrans__()));
                }, false);
            }),
            new Source(b.getVertex__(), null),
            new Source(c.getVertex__(), null),
            new Source(d.getVertex__(), null),
            new Source(e.getVertex__(), null),
            new Source(f.getVertex__(), null)
        ].concat(toSources(Lambda6_deps(f_))));
        return out;
    }
    /**
     * Create a {@link Cell} with the specified initial value, that is updated
     * by this stream's event values.
     * <p>
     * There is an implicit delay: State updates caused by event firings don't become
     * visible as the cell's current value as viewed by {@link Stream#snapshot(Cell, Lambda2)}
     * until the following transaction. To put this another way,
     * {@link Stream#snapshot(Cell, Lambda2)} always sees the value of a cell as it was before
     * any state changes from the current transaction.
     */
    hold(initValue) {
        return new Cell(initValue, this);
    }
    /**
     * A variant of {@link hold(Object)} with an initial value captured by {@link Cell#sampleLazy()}.
     */
////holdLazy(initValue) {
////    return new LazyCell(initValue, this);
////} 20181205^RP removed circular dependency
    holdLazy(initValue) {
        return new LazyCell(initValue, this);
    }
    /**
     * Transform an event with a generalized state loop (a Mealy machine). The function
     * is passed the input and the old state and returns the new state and output value.
     * @param f Function to apply to update the state. It may construct FRP logic or use
     *    {@link Cell#sample()} in which case it is equivalent to {@link Stream#snapshot(Cell)}ing the
     *    cell. Apart from this the function must be <em>referentially transparent</em>.
     */
    collect(initState, f) {
        return this.collectLazy(new Lazy(() => { return initState; }), f);
    }
    /**
     * A variant of {@link collect(Object, Lambda2)} that takes an initial state returned by
     * {@link Cell#sampleLazy()}.
     */
    collectLazy(initState, f) {
        const ea = this;
        return Transaction.run(() => {
            const es = new StreamLoop(), s = es.holdLazy(initState), ebs = ea.snapshot(s, f), eb = ebs.map((bs) => { return bs.a; }), es_out = ebs.map((bs) => { return bs.b; });
            es.loop(es_out);
            return eb;
        });
    }
    /**
     * Accumulate on input event, outputting the new state each time.
     * @param f Function to apply to update the state. It may construct FRP logic or use
     *    {@link Cell#sample()} in which case it is equivalent to {@link Stream#snapshot(Cell)}ing the
     *    cell. Apart from this the function must be <em>referentially transparent</em>.
     */
    accum(initState, f) {
        return this.accumLazy(new Lazy(() => { return initState; }), f);
    }
    /**
     * A variant of {@link accum(Object, Lambda2)} that takes an initial state returned by
     * {@link Cell#sampleLazy()}.
     */
    accumLazy(initState, f) {
        const ea = this;
        return Transaction.run(() => {
            const es = new StreamLoop(), s = es.holdLazy(initState), es_out = ea.snapshot(s, f);
            es.loop(es_out);
            return es_out.holdLazy(initState);
        });
    }
    /**
     * Return a stream that outputs only one value: the next event of the
     * input stream, starting from the transaction in which once() was invoked.
     */
    once() {
        /*
            return Transaction.run(() => {
                const ev = this,
                    out = new StreamWithSend<A>();
                let la : () => void = null;
                la = ev.listen_(out.vertex, (a : A) => {
                    if (la !== null) {
                        out.send_(a);
                        la();
                        la = null;
                    }
                }, false);
                return out;
            });
            */
        // We can't use the implementation above, beacuse deregistering
        // listeners triggers the exception
        // "send() was invoked before listeners were registered"
        // We can revisit this another time. For now we will use the less
        // efficient implementation below.
        const me = this;
        return Transaction.run(() => me.gate(me.mapTo(false).hold(true)));
    }
    listen(h) {
        return Transaction.run(() => {
            return this.listen_(Vertex.NULL, h, false);
        });
    }
    listen_(target, h, suppressEarlierFirings) {
        if (this.vertex.register(target))
            Transaction.currentTransaction.requestRegen();
        const listener = new Listener(h, target);
        this.listeners.push(listener);
        if (!suppressEarlierFirings && this.firings.length != 0) {
            const firings = this.firings.slice();
            Transaction.currentTransaction.prioritized(target, () => {
                // Anything sent already in this transaction must be sent now so that
                // there's no order dependency between send and listen.
                for (let i = 0; i < firings.length; i++)
                    h(firings[i]);
            });
        }
        return () => {
            let removed = false;
            for (let i = 0; i < this.listeners.length; i++) {
                if (this.listeners[i] == listener) {
                    this.listeners.splice(i, 1);
                    removed = true;
                    break;
                }
            }
            if (removed)
                this.vertex.deregister(target);
        };
    }
    /**
     * Fantasy-land Algebraic Data Type Compatability.
     * Stream satisfies the Functor and Monoid Categories (and hence Semigroup)
     * @see {@link https://github.com/fantasyland/fantasy-land} for more info
     */
    //map :: Functor f => f a ~> (a -> b) -> f b
    'fantasy-land/map'(f) {
        return this.map(f);
    }
    //concat :: Semigroup a => a ~> a -> a
    'fantasy-land/concat'(a) {
        return this.merge(a, (left, right) => {
            return (Z.Semigroup.test(left)) ? Z.concat(left, right) : left;
        });
    }
    //empty :: Monoid m => () -> m
    'fantasy-land/empty'() {
        return new Stream();
    }
}
export class StreamWithSend extends Stream {
    constructor(vertex) {
        super(vertex);
    }
    setVertex__(vertex) {
        this.vertex = vertex;
    }
    send_(a) {
        // We throw this error if we send into FRP logic that has been constructed
        // but nothing is listening to it yet. We need to do it this way because
        // it's the only way to manage memory in a language with no finalizers.
        if (this.vertex.refCount() == 0)
            throw new Error("send() was invoked before listeners were registered");
        if (this.firings.length == 0)
            Transaction.currentTransaction.last(() => {
                this.firings = [];
            });
        this.firings.push(a);
        const listeners = this.listeners.slice();
        for (let i = 0; i < listeners.length; i++) {
            const h = listeners[i].h;
            Transaction.currentTransaction.prioritized(listeners[i].target, () => {
                Transaction.currentTransaction.inCallback++;
                try {
                    h(a);
                    Transaction.currentTransaction.inCallback--;
                }
                catch (err) {
                    Transaction.currentTransaction.inCallback--;
                    throw err;
                }
            });
        }
    }
}
/**
 * A forward reference for a {@link Stream} equivalent to the Stream that is referenced.
 */
export class StreamLoop extends StreamWithSend {
    constructor() {
        super();
        this.assigned__ = false; // to do: Figure out how to hide this
        this.vertex.name = "StreamLoop";
        if (Transaction.currentTransaction === null)
            throw new Error("StreamLoop/CellLoop must be used within an explicit transaction");
    }
    /**
     * Resolve the loop to specify what the StreamLoop was a forward reference to. It
     * must be invoked inside the same transaction as the place where the StreamLoop is used.
     * This requires you to create an explicit transaction with {@link Transaction#run(Lambda0)}
     * or {@link Transaction#runVoid(Runnable)}.
     */
    loop(sa_out) {
        if (this.assigned__)
            throw new Error("StreamLoop looped more than once");
        this.assigned__ = true;
        this.vertex.addSource(new Source(sa_out.getVertex__(), () => {
            return sa_out.listen_(this.vertex, (a) => {
                this.send_(a);
            }, false);
        }));
    }
}


//// support/convert-sodium-ts-examples-to-mjs.js converted to .mjs.
if ('object' === typeof window && 'function' === typeof CustomEvent)
    window.dispatchEvent( new CustomEvent('lib-module-loaded', {
        detail: 'lib/sodiumjs/stream.mjs' }) )
