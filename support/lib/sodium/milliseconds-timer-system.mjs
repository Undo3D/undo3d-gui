import { TimerSystem, TimerSystemImpl } from './timer-system.mjs'
/**
 * A timer system implementation using milliseconds as the time unit.
 */
export class MillisecondsTimerSystem extends TimerSystem {
    constructor() {
        super(new MillisecondsTimerSystemImpl());
    }
}
class MillisecondsTimerSystemImpl extends TimerSystemImpl {
    /**
     * Set a timer that will execute the specified callback at the specified time.
     * @return A function that can be used to cancel the timer.
     */
    setTimer(t, callback) {
        let timeout = setTimeout(callback, Math.max(t - this.now(), 0));
        return () => { clearTimeout(timeout); };
    }
    /**
     * Return the current clock time.
     */
    now() {
        return Date.now();
    }
}


//// support/convert-sodium-ts-examples-to-mjs.js converted to .mjs.
if ('object' === typeof window && 'function' === typeof CustomEvent)
    window.dispatchEvent( new CustomEvent('lib-module-loaded', {
        detail: 'lib/sodiumjs/milliseconds-timer-system.mjs' }) )