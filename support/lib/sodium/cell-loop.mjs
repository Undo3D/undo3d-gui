import { LazyCell } from './lazy-cell.mjs'
import { Transaction } from './transaction.mjs'
import { StreamLoop } from './stream.mjs'
/**
 * A forward reference for a {@link Cell} equivalent to the Cell that is referenced.
 */
export class CellLoop extends LazyCell {
    constructor() {
        super(null, new StreamLoop());
    }
    /**
     * Resolve the loop to specify what the CellLoop was a forward reference to. It
     * must be invoked inside the same transaction as the place where the CellLoop is used.
     * This requires you to create an explicit transaction with {@link Transaction#run(Lambda0)}
     * or {@link Transaction#runVoid(Runnable)}.
     */
    loop(a_out) {
        const me = this;
        Transaction.run(() => {
            me.getStream__().loop(a_out.getStream__());
            me.lazyInitValue = a_out.sampleLazy();
        });
    }
    sampleNoTrans__() {
        if (!this.getStream__().assigned__)
            throw new Error("CellLoop sampled before it was looped");
        return super.sampleNoTrans__();
    }
}


//// support/convert-sodium-ts-examples-to-mjs.js converted to .mjs.
if ('object' === typeof window && 'function' === typeof CustomEvent)
    window.dispatchEvent( new CustomEvent('lib-module-loaded', {
        detail: 'lib/sodiumjs/cell-loop.mjs' }) )