import { StreamWithSend } from './stream.mjs'
import { CoalesceHandler } from './coalesce-handler.mjs'
import { Transaction } from './transaction.mjs'
/**
 * A stream that allows values to be pushed into it, acting as an interface between the
 * world of I/O and the world of FRP. Code that exports StreamSinks for read-only use
 * should downcast to {@link Stream}.
 */
export class StreamSink extends StreamWithSend {
    constructor(f) {
        super();
        if (!f)
            f = ((l, r) => {
                throw new Error("send() called more than once per transaction, which isn't allowed. Did you want to combine the events? Then pass a combining function to your StreamSink constructor.");
            });
        this.coalescer = new CoalesceHandler(f, this);
    }
    send(a) {
        Transaction.run(() => {
            if (Transaction.currentTransaction.inCallback > 0)
                throw new Error("You are not allowed to use send() inside a Sodium callback");
            this.coalescer.send_(a);
        });
    }
}


//// support/convert-sodium-ts-examples-to-mjs.js converted to .mjs.
if ('object' === typeof window && 'function' === typeof CustomEvent)
    window.dispatchEvent( new CustomEvent('lib-module-loaded', {
        detail: 'lib/sodiumjs/stream-sink.mjs' }) )