import { Cell } from './cell.mjs'
import { Transaction } from './transaction.mjs'
export class LazyCell extends Cell {
    constructor(lazyInitValue, str) {
        super(null, null);
        Transaction.run(() => {
            if (str)
                this.setStream(str);
            this.lazyInitValue = lazyInitValue;
        });
    }
    sampleNoTrans__() {
        if (this.value == null && this.lazyInitValue != null) {
            this.value = this.lazyInitValue.get();
            this.lazyInitValue = null;
        }
        return this.value;
    }
}


//// support/convert-sodium-ts-examples-to-mjs.js converted to .mjs.
if ('object' === typeof window && 'function' === typeof CustomEvent)
    window.dispatchEvent( new CustomEvent('lib-module-loaded', {
        detail: 'lib/sodiumjs/lazy-cell.mjs' }) )