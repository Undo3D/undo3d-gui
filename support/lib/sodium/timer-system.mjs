import { Vertex, Source } from './vertex.mjs'
import * as Collections from '../typescript-collections/index.mjs';
import { StreamWithSend } from './stream.mjs'
import { CellSink } from './cell-sink.mjs'
import { Transaction } from './transaction.mjs'
/**
 * An interface for implementations of FRP timer systems.
 */
export class TimerSystemImpl {
}
let nextSeq = 0;
class Event {
    constructor(t, sAlarm) {
        this.t = t;
        this.sAlarm = sAlarm;
        this.seq = ++nextSeq;
    }
}
export class TimerSystem {
    constructor(impl) {
        this.eventQueue = new Collections.BSTree((a, b) => {
            if (a.t < b.t)
                return -1;
            if (a.t > b.t)
                return 1;
            if (a.seq < b.seq)
                return -1;
            if (a.seq > b.seq)
                return 1;
            return 0;
        });
        Transaction.run(() => {
            this.impl = impl;
            this.tMinimum = 0;
            const timeSnk = new CellSink(impl.now());
            this.time = timeSnk;
            // A dummy listener to time to keep it alive even when there are no other listeners.
            this.time.listen((t) => { });
            Transaction.onStart(() => {
                // Ensure the time is always increasing from the FRP's point of view.
                const t = this.tMinimum = Math.max(this.tMinimum, impl.now());
                // Pop and execute all events earlier than or equal to t (the current time).
                while (true) {
                    let ev = null;
                    if (!this.eventQueue.isEmpty()) {
                        let mev = this.eventQueue.minimum();
                        if (mev.t <= t) {
                            ev = mev;
                            // TO DO: Detect infinite loops!
                        }
                    }
                    if (ev != null) {
                        timeSnk.send(ev.t);
                        Transaction.run(() => ev.sAlarm.send_(ev.t));
                    }
                    else
                        break;
                }
                timeSnk.send(t);
            });
        });
    }
    /**
     * A timer that fires at the specified time, which can be null, meaning
     * that the alarm is not set.
     */
    at(tAlarm) {
        let current = null, cancelCurrent = null, active = false, tAl = null, sampled = false;
        const sAlarm = new StreamWithSend(null), updateTimer = () => {
            if (cancelCurrent !== null) {
                cancelCurrent();
                this.eventQueue.remove(current);
            }
            cancelCurrent = null;
            current = null;
            if (active) {
                if (!sampled) {
                    sampled = true;
                    tAl = tAlarm.sampleNoTrans__();
                }
                if (tAl !== null) {
                    current = new Event(tAl, sAlarm);
                    this.eventQueue.add(current);
                    cancelCurrent = this.impl.setTimer(tAl, () => {
                        // Correction to ensure the clock time appears to be >= the
                        // alarm time. It can be a few milliseconds early, and
                        // this breaks things otherwise, because it doesn't think
                        // it's time to fire the alarm yet.
                        this.tMinimum = Math.max(this.tMinimum, tAl);
                        // Open and close a transaction to trigger queued
                        // events to run.
                        Transaction.run(() => { });
                    });
                }
            }
        };
        sAlarm.setVertex__(new Vertex("at", 0, [
            new Source(tAlarm.getVertex__(), () => {
                active = true;
                sampled = false;
                Transaction.currentTransaction.prioritized(sAlarm.getVertex__(), updateTimer);
                const kill = tAlarm.getStream__().listen_(sAlarm.getVertex__(), (oAlarm) => {
                    tAl = oAlarm;
                    sampled = true;
                    updateTimer();
                }, false);
                return () => {
                    active = false;
                    updateTimer();
                    kill();
                };
            })
        ]));
        return sAlarm;
    }
}


//// support/convert-sodium-ts-examples-to-mjs.js converted to .mjs.
if ('object' === typeof window && 'function' === typeof CustomEvent)
    window.dispatchEvent( new CustomEvent('lib-module-loaded', {
        detail: 'lib/sodiumjs/timer-system.mjs' }) )