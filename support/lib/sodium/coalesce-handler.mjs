import { Lambda2_deps, Lambda2_toFunction, toSources } from './lambda.mjs'
import { Transaction } from './transaction.mjs'
export class CoalesceHandler {
    constructor(f, out) {
        this.f = Lambda2_toFunction(f);
        this.out = out;
        this.out.getVertex__().sources = this.out.getVertex__().sources.concat(toSources(Lambda2_deps(f)));
        this.accumValid = false;
    }
    send_(a) {
        if (this.accumValid)
            this.accum = this.f(this.accum, a);
        else {
            Transaction.currentTransaction.prioritized(this.out.getVertex__(), () => {
                this.out.send_(this.accum);
                this.accumValid = false;
                this.accum = null;
            });
            this.accum = a;
            this.accumValid = true;
        }
    }
}


//// support/convert-sodium-ts-examples-to-mjs.js converted to .mjs.
if ('object' === typeof window && 'function' === typeof CustomEvent)
    window.dispatchEvent( new CustomEvent('lib-module-loaded', {
        detail: 'lib/sodiumjs/coalesce-handler.mjs' }) )