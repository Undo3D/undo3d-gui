# Process for converting typescript-collections to .mjs files:

1. Visit https://github.com/basarat/typescript-collections
2. Download the zip file and unzip it
3. `$ npm install -g typescript`
4. `$ cd <path/to/unzipped/repo>/typescript-collections-release/src/lib`
5. `$ tsc *.ts --target ES6`
6. `$ mkdir <path/to/this/project>/lib/typescript-collections`
7. `$ mv *.js <path/to/this/project>/lib/typescript-collections`
8. `$ cd <path/to/this/project>`
9. `$ node support/convert-tscollections-to-mjs.js`
10. Manually correct the paths in index.mjs
