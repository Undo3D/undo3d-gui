// Copyright 2013 Basarat Ali Syed. All Rights Reserved.
//
// Licensed under MIT open source license http://opensource.org/licenses/MIT
//
// Orginal javascript code was by Mauricio Santos
//
import * as _arrays from './arrays.mjs'
export var arrays = _arrays;
export { default as Bag } from './bag.mjs';
export { default as BSTree } from './b-s-tree.mjs';
export { default as BSTreeKV } from './b-s-tree-k-v.mjs';
export { default as Dictionary } from './dictionary.mjs';
export { default as Heap } from './heap.mjs';
export { default as LinkedDictionary } from './linked-dictionary.mjs';
export { default as LinkedList } from './linked-list.mjs';
export { default as MultiDictionary } from './multi-dictionary.mjs';
export { default as FactoryDictionary } from './factory-dictionary.mjs';
export { default as DefaultDictionary } from './factory-dictionary.mjs';
export { default as Queue } from './queue.mjs';
export { default as PriorityQueue } from './priority-queue.mjs';
export { default as Set } from './set.mjs';
export { default as Stack } from './stack.mjs';
export { default as MultiRootTree } from './multi-root-tree.mjs';
import * as _util from './util.mjs'
export var util = _util;

//// support/convert-tscollections-to-mjs.js converted to .mjs.
if ('object' === typeof window && 'function' === typeof CustomEvent)
    window.dispatchEvent( new CustomEvent('lib-module-loaded', {
        detail: 'lib/typescript-collections/index.mjs' }) )
