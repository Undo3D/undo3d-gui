//// Commandeer console.log() and console.error(). The in-window log is less
//// fussy than the browser console, and works on mobile devices.

if ('object' === typeof document) {

    //// Get references to the native log() and error() before overwriting them.
    const
        oldLog = console.log.bind(console)
      , oldError = console.error.bind(console)

    //// Get a ref to the output HTML element, and clear it.
    const $console = document.querySelector('#console')
    $console.innerHTML = ''

    ////@TODO pretty-format
    console.log = function (...args) {
        $console.innerHTML += args.join(' ') + '\n'
        oldLog(...args)
    }

    //// Errors are shown in bold (use CSS to make them red).
    console.error = function (...args) {
        function formatError (error) {
            return error.name + ':\n  '
                 + error.message + '\n  '
                 + error.stack.split('\n')[0].split('/').slice(-1)
        }
        if (1 === args.length && args[0] instanceof Error)
            $console.innerHTML += '<b>' + formatError(args[0]) + '</b>\n'
        else
            $console.innerHTML += '<b>' + args.join(' ') + '</b>\n'
        oldError(...args)
    }

}
