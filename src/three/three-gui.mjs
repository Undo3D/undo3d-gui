import Gui from '../utility/gui.mjs'
import InputElement from '../utility/input-element.mjs' // used for `instanceof InputElement`
import ThreeButton from './elements/three-button.mjs'
import ThreeText from './elements/three-text.mjs'
import ThreeInputText from './elements/three-input-text.mjs'
import ThreeRenderer from './three-renderer.mjs'

export default class ThreeGui extends Gui {

    //// Override placeholder Gui properties.
    static get Button () { return ThreeButton }
    static get Text () { return ThreeText }
    static get InputText () { return ThreeInputText }
    static get Renderer () { return ThreeRenderer }

    constructor (options) {
        super(options)

        this._currentFocus = this.current.focus
        Object.defineProperty(this.current, 'focus', {
            get: () => { return this._currentFocus }
          , set: newFocusEl => {
                if (newFocusEl === this._currentFocus) return newFocusEl
                if (this._currentFocus) this._currentFocus.setFocus(false)
                if (newFocusEl) newFocusEl.setFocus(true)
                return this._currentFocus = newFocusEl
            }
        })

        this._currentActive = this.current.active
        Object.defineProperty(this.current, 'active', {
            get: () => { return this._currentActive }
          , set: newActiveEl => {
                if (newActiveEl === this._currentActive) return newActiveEl
                if (this._currentActive) this._currentActive.setActive(false)
                if (newActiveEl) newActiveEl.setActive(true)
                return this._currentActive = newActiveEl
            }
        })

        //// Used for picking the target of mouse/touch events.
        //// based on https://threejs.org/docs/#api/en/core/Raycaster
        this.raycaster = new THREE.Raycaster()
        this.mouse = new THREE.Vector2()

        const
            { selector, mouse, raycaster, renderer, current } = this
          , $canvas = document.querySelector(selector)
          , passEventToElement = evt => {

                //// Add `x` and `y` to the event object, if this is a touch.
                const touches = evt.changedTouches || evt.targetTouches || null
                if (touches)
                    if (0 === touches.length)
                        return
                    else
                        evt.x = touches[0].clientX //@TODO deal with multiple touches
                      , evt.y = touches[0].clientY //@TODO deal with multiple touches

                // based on https://threejs.org/docs/#api/en/core/Raycaster
            	// calculate mouse position in normalized device coordinates
            	// (-1 to +1) for both components
                const
                    { x, y, width, height } = $canvas.getBoundingClientRect()
                  , X = evt.x - x
                  , Y = evt.y - y
                mouse.x =   (X / width)  * 2 - 1
                mouse.y = - (Y / height) * 2 + 1

            	// update the picking ray with the camera and mouse position
            	raycaster.setFromCamera(mouse, renderer.camera)

            	//// Choose the first object intersecting the picking ray. @TODO frontmost?
            	const intersects = raycaster.intersectObjects(renderer.scene.children)
                const frontmost_type_id =
                    intersects.length ? intersects[0].object.userData.undo3d_id : null
                const frontmost_id =
                    frontmost_type_id ? frontmost_type_id.split('_')[1] : null
                let target = null
                if (frontmost_id)
                    for (const element of this.elements)
                        if (element.id === frontmost_id) {
                            target = element
                            break
                        }

                //// Derive 'mouseover' and 'mouseout' from 'mousemove'.
                let mouseoutElement = null
                let mouseoverElement = null
                let mousemoveElement = null
                if ('mousemove' === evt.type) {
                    if (null === target && null === current.hover) { // continued moving over nothing
                        mousemoveElement = null
                    } else if (target === current.hover) { // moving over current hover-element
                        mousemoveElement = target
                    } else if (null === target && current.hover) { // moved from an element to nothing
                        mouseoutElement = current.hover
                        mousemoveElement = null
                    } else if (target && null === current.hover) { // moved from nothing to an element
                        mouseoverElement = target
                        mousemoveElement = target
                    } else if (target && current.hover) { // moved from one element to another
                        mouseoutElement = current.hover
                        mouseoverElement = target
                        mousemoveElement = target
                    }
                    if (mouseoutElement)  mouseoutElement.fire('mouseout')
                    if (mouseoverElement) mouseoverElement.fire('mouseover')
                    if (mousemoveElement) mousemoveElement.fire('mousemove')
                    current.hover = mousemoveElement // could be null

                //// Deal with events which alter 'active' and 'focus' state.
                } else if (target) {
                    if (target instanceof InputElement)
                        if ('mousedown' === evt.type || 'touchstart' === evt.type) {
                            if (current.focus && current.focus !== target && current.focus.setFocus)
                                current.focus.setFocus(false) //@TODO remove this?
                            if (target instanceof ThreeButton) {
                                current.focus = null
                            } else {
                                if (target.setFocus)
                                    target.setFocus(true) //@TODO remove this?
                                current.focus = target
                            }
                            if (target.setActive)
                                target.setActive(true)
                            current.active = target
                        } else if ('mouseup' === evt.type || 'touchend' === evt.type) {
                            if (current.active && current.active.setActive)
                                current.active.setActive(false)
                            current.active = null
                        }
                    target.fire(evt.type)
                    this.renderer.render()

                } else { // no target
                    if ('mousedown' === evt.type || 'touchstart' === evt.type) {
                        if (current.focus && current.focus.setActive)
                            current.focus.setFocus(false) //@TODO remove this?
                        if (current.active && current.active.setActive)
                            current.active.setActive(false)
                        current.focus = null
                        current.active = null
                        this.renderer.render()
                    }
                }

            }
        $canvas.addEventListener('mousedown',  passEventToElement)
        $canvas.addEventListener('mouseup',    passEventToElement)
        $canvas.addEventListener('touchstart', passEventToElement)
        $canvas.addEventListener('touchend',   passEventToElement)
        $canvas.addEventListener('mousemove',  passEventToElement)

        //// End hover if the user switches window focus.
        window.addEventListener('blur', evt => {
            if (null === current.hover) return
            current.hover.fire('mouseout')
            current.hover = null
        })

    }




//// Inherited methods:
///
//  addButton (options)
//  addText (options)
//  render ()

}
