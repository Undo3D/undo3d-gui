import InputElement from '../../utility/input-element.mjs'

export default class ThreeInputText extends InputElement {

    constructor (options, frpLib, renderer) {
        super(options, frpLib, renderer)
        this.isActive = false
        this.isFocus = false
        this.value = this.value || '' // super has set the value to options.value - but it may be undefined
        const
            { id, placeholder, x, y, w, isActive, isFocus } = this
          , z = 0, h = 40, d = 2, b = 2
          , fontFilename = 'helvetiker_regular.typeface.json'
        renderer.addBox({
            id, fill:'#213', stroke:'#5599F8', x, y, z, w, h, d, b, tag:'input-text'
          , isActive, isFocus })
        renderer.addText({
            id, fill:this.value?'#5599F8':'#3366AA', text:this.value||placeholder
          , fontFilename, x:x+10, y:y+19, z:z+1.1, align:'left'
          , isActive, tag:'input-text' })
    }

    // setText (options) {
    //     this.text = options.text
    //     const
    //         { id, placeholder, x, y, w, renderer } = this
    //       , z = 0
    //       , fontFilename = 'helvetiker_regular.typeface.json'
    //     renderer.replaceText({ id, fill:this.value?'#5599F8':'#3366AA', text:this.value||placeholder, fontFilename, x:x+(w/2), y:y+19, z:z+1.1, align:'left' })
    //     renderer.render()
    // }

    setActive (isActive) {
        const
            { id, placeholder, x, y, w, isFocus, renderer } = this
          , z = 0, h = 40, d = 2, b = 2
          , fontFilename = 'helvetiker_regular.typeface.json'
        if (isActive === this.isActive) return // no change
        this.isActive = isActive
        renderer.replaceBox({
            id, fill:'#213', stroke:'#5599F8', x, y, z, w, h, d, b, tag:'input-text'
          , isActive, isFocus })
        renderer.replaceText({
            id, fill:this.value?'#5599F8':'#3366AA', text:this.value||placeholder
          , fontFilename, x:x+10, y:y+19, z:z+1.1, b, align:'left'
          , isActive, tag:'input-text' })

        renderer.render()
    }

    setFocus (isFocus) {
        const
            { id, x, y, w, isActive, renderer } = this
          , z = 0, h = 40, d = 2, b = 2
        if (isFocus === this.isFocus) return // no change
        this.isFocus = isFocus
        renderer.replaceBox({
            id, fill:'#213', stroke:'#5599F8', x, y, z, w, h, d, b, tag:'input-text'
          , isActive, isFocus })
        renderer.render()
    }

//// Inherited methods:
///
//  on ()

}
