import InputElement from '../../utility/input-element.mjs'

export default class ThreeButton extends InputElement {

    constructor (options, frpLib, renderer) {
        super(options, frpLib, renderer)
        this.isActive = false
        this.isFocus = false
        const
            { id, text, x, y, w, isActive, isFocus } = this
          , z = 0, h = 40, d = 2, b = 2
          , fontFilename = 'helvetiker_regular.typeface.json'
        renderer.addBox({
            id, fill:'#213', stroke:'#5599F8', x, y, z, w, h, d, b, tag:'button'
          , isActive, isFocus })
        renderer.addText({
            id, fill:'#5599F8', text, fontFilename, x:x+(w/2), y:y+19, z:z+1.1, b
          , isActive, tag:'button' })
    }

    setText (options) {
        this.text = options.text
        const
            { id, text, x, y, w, renderer } = this
          , z = 0
          , fontFilename = 'helvetiker_regular.typeface.json'
        renderer.replaceText({
            id, fill:'#5599F8', text, fontFilename, x:x+(w/2), y:y+19, z:z+1.1, b
          , tag:'button' })
        renderer.render()
    }

    setActive (isActive) {
        const
            { id, text, x, y, w, isFocus, renderer } = this
          , z = 0, h = 40, d = 2, b = 2
          , fontFilename = 'helvetiker_regular.typeface.json'
        if (isActive === this.isActive) return // no change
        this.isActive = isActive
        renderer.replaceBox({
            id, fill:'#213', stroke:'#5599F8', x, y, z, w, h, d, b, tag:'button'
          , isActive, isFocus })
        renderer.replaceText({
            id, fill:'#5599F8', text, fontFilename, x:x+(w/2), y:y+19, z:z+1.1, b
          , isActive, tag:'button' }) // note +21 not +19
        renderer.render()
    }

    setFocus (isFocus) {
        const
            { id, x, y, w, isActive, renderer } = this
          , z = 0, h = 40, d = 2, b = 2
        if (isFocus === this.isFocus) return // no change
        this.isFocus = isFocus
        renderer.replaceBox({
            id, fill:'#213', stroke:'#5599F8', x, y, z, w, h, d, b, tag:'button'
          , isActive, isFocus })
        renderer.render()
    }

//// Inherited methods:
///
//  on ()

}
