import Element from '../../utility/element.mjs'

export default class ThreeText extends Element {

    constructor (options, frpLib, renderer) {
        super(options, frpLib, renderer)
        const
            { id, text, x, y, w } = this
          , z = 0
          , fontFilename = 'helvetiker_regular.typeface.json'
        renderer.addText({ id, fill:'#5599F8', text, fontFilename, x:x+(w/2), y:y+19, z:z+1.1 })
    }

    setText (options) {
        this.text = options.text
        const
            { id, text, x, y, w, renderer } = this
          , z = 0
          , fontFilename = 'helvetiker_regular.typeface.json'
        renderer.replaceText({ id, fill:'#5599F8', text, fontFilename, x:x+(w/2), y:y+19, z:z+1.1 })
        renderer.render()
    }

//// Inherited methods:
///
//  on ()

}
