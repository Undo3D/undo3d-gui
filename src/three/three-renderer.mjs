export default class CanvasRenderer {

    constructor (options) { // { w, h, d, selector }
        for ( let [ key, value ] of Object.entries(options) ) this[key] = value
        const
            { w, h, d, selector } = this
          , S = 0.1 // scale, converts pixels to meters
          , W = w*S, H = h*S, D = d*S

        //// Used for changing text (actually, replacing it).
        this.textMeshes = {} // by id
        this.boxMeshes = {} // by id

        //// Caches, used by getGeometry() and getMaterial().
        this.fonts = {}
        this.geometries = {}
        this.materials = {}

        //// Scene and Camera.
        this.$canvas = document.querySelector(selector)
        this.scene = new THREE.Scene()
        this.camera = new THREE.PerspectiveCamera(75, w/h, 0.1, 1000)
        this.camera.position.set(W*0.5, -H*0.5, D*1.3)
        this.scene.add(this.camera)
        // this.camera.lookAt(W/2, -H/2, 0)

        //// Renderer. @TODO rename to avoid confusion
        this.renderer = new THREE.WebGLRenderer({
            canvas:this.$canvas, antialias:true, alpha:true
        })
    	// this.renderer.setPixelRatio(config.pixelRatio)
    	// this.renderer.autoClear = false
        this.renderer.setSize(w, h)
    }

    getGeometry (W, H, D) {
        return this.geometries[`${W},${H},${D}`]
         || (this.geometries[`${W},${H},${D}`] = new THREE.BoxGeometry(W, H, D))
    }
    getMaterial (color) {
        return this.materials[color+'']
         || (this.materials[color+''] = new THREE.MeshBasicMaterial({ color }))
    }

    replaceText ({ id, fill, text, x, y, z, b, fontFilename, align, tag, isActive }) {
        if (this.textMeshes[id])
            this.scene.remove(this.textMeshes[id])
        this.addText({ id, fill, text, x, y, z, b, fontFilename, align, tag, isActive })
    }

    replaceBox ({ id, fill, stroke, x, y, z, w, h, d, b, tag, isActive, isFocus }) {
        if (this.boxMeshes[id])
            this.boxMeshes[id].forEach( mesh => this.scene.remove(mesh) )
        this.addBox({ id, fill, stroke, x, y, z, w, h, d, b, tag, isActive, isFocus })
    }

    addText ({ id, fill, text, x, y, z, b, fontFilename, align, tag, isActive }) {
        const
            { assetPath } = this
          , isButton    = 'button' === tag
          , isInputText = 'input-text' === tag
          , fontPath = assetPath + fontFilename
          , S = 0.1
          , B = b*S // B is border thickness in meters
          , X = x*S
          , Y = -y*S - (isActive ? B : 0)
          , Z = z*S
          , loader = new THREE.FontLoader()
          , material = this.getMaterial(fill)
          , addLineOfText = font => {
                const
                    geometry = new THREE.TextGeometry(text, {
                        font
                      , size: 1.333
                      , height: 0
                      , curveSegments: 3
                      , bevelEnabled: false
                    })
                  , mesh = new THREE.Mesh(geometry, material)
                mesh.userData = { undo3d_id:'text_'+id }
                geometry.computeBoundingBox()
                const w = geometry.boundingBox.max.x - geometry.boundingBox.min.x
                mesh.position.set('left'===align?X:X-w/2, Y-0.7, Z)
                this.scene.add(mesh)
                this.renderer.render(this.scene, this.camera)
                this.textMeshes[id] = mesh
            }
        if (this.fonts[fontPath]) //@TODO also wait if pending
            addLineOfText(this.fonts[fontPath])
        else
            loader.load(fontPath, font => {
                this.fonts[fontPath] = font // cache, the first time
                addLineOfText(font)
            })
    }

    addBox ({ id, fill, stroke, x, y, z, w, h, d, b, tag, isActive, isFocus }) {
        const
            { context, current } = this
          , isButton    = 'button' === tag
          , isInputText = 'input-text' === tag
          , S = 0.1
          , B = b*S // B is border thickness in meters
          , BT = isActive && isInputText ? B*2 : B // border-top thickness
          , BB = ! isActive && isButton  ? B*2 : B // border-bottom thickness
          , X = x*S
          , XT = isActive && isInputText ? X+B/2 : X // border-top x-position
          , XB = ! isActive && isButton  ? X+B/2 : X // border-bottom x-position
          , Y = -y*S - (isActive && isButton ? B : 0)
          , YT = isActive && isInputText ? Y - B/2 : Y
          , Z = z*S
          , W = w*S
          , WT = isActive && isInputText ? W-B : W // border-top width
          , WB = ! isActive && isButton  ? W-B : W // border-bottom width
          , H = h*S
          , D = d*S
          , LZ = Z+B // bring lines forward a little
          , meshes = []
        this.boxMeshes[id] = meshes
        id = 'box_'+id
        meshes.push( this.addHorizontalLine({ id, stroke, X:XT, Y:YT,  Z:LZ, W:WT, B:BT }) )
        meshes.push( this.addHorizontalLine({ id, stroke, X:XB, Y:Y-H, Z:LZ, W:WB, B:BB }) )
        meshes.push( this.addVerticalLine(  { id, stroke, X,       Y,     Z:LZ,        H,   B }) )
        meshes.push( this.addVerticalLine(  { id, stroke, X:X+W,   Y,     Z:LZ,        H,   B }) )
        meshes.push( this.addBlock(         { id, fill,   X,       Y,     Z,    W,     H, D }) )
        if (isFocus) {
            const
                YB  = ! isActive && isButton ? Y-H-B*3 : Y-H-B*2
              , LRH = ! isActive && isButton ? H+B*4   : H+B*3
            meshes.push( this.addHorizontalLine({ id, stroke, X:X-B*2,   Y:Y+B*2,   Z:LZ, W:W+B*4, B }) )
            meshes.push( this.addHorizontalLine({ id, stroke, X:X-B*2,   Y:YB,      Z:LZ, W:W+B*4, B }) )
            meshes.push( this.addVerticalLine(  { id, stroke, X:X-B*2,   Y:Y+B*1.5, Z:LZ, H:LRH,   B }) )
            meshes.push( this.addVerticalLine(  { id, stroke, X:X+W+B*2, Y:Y+B*1.5, Z:LZ, H:LRH,   B }) )
        }
        return null; //@TODO make this a useful object ref
    }

    addHorizontalLine ({ id, stroke, X, Y, Z, W, B }) {
        const
            { context } = this
          , geometry = this.getGeometry(W+B, B, B/5)
          , material = this.getMaterial(stroke)
          , mesh = new THREE.Mesh(geometry, material)
        mesh.userData = { undo3d_id:id }
        mesh.position.set(X+W/2, Y, Z)
        this.scene.add(mesh)
        return mesh
    }

    addVerticalLine ({ id, stroke, X, Y, Z, H, B }) {
        const
            { context } = this
          , geometry = this.getGeometry(B, H, B/5)
          , material = this.getMaterial(stroke)
          , mesh = new THREE.Mesh(geometry, material)
        mesh.userData = { undo3d_id:id }
        mesh.position.set(X, Y-H/2, Z)
        this.scene.add(mesh)
        return mesh
    }

    addBlock ({ id, fill, X, Y, Z, H, W, D }) {
        const
            { context } = this
          , geometry = this.getGeometry(W, H, D)
          , material = this.getMaterial(fill)
          , mesh = new THREE.Mesh(geometry, material)
        mesh.userData = { undo3d_id:id }
        mesh.position.set(X+W/2, Y-H/2, Z)
        this.scene.add(mesh)
        return mesh
    }

    render () {
        this.renderer.render(this.scene, this.camera)
    }

}
