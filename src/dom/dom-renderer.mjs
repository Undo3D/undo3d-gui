export default class DomRenderer {

    constructor (options) { // { w, h, d, selector }
        for ( let [ key, value ] of Object.entries(options) ) this[key] = value
        const { selector } = this
        this.$el = document.querySelector(selector)
    }

    addText ({ id, text, fill, x, y, w, h, font, align, tag }) {
        const
            $text = document.createElement('div')
          , style = {
                position: 'absolute'
              , color: fill
              , marginLeft: x + 'px'
              , marginTop: y + 'px'
              , width: w + 'px'
              , height: h + 'px'
              , font
              , textAlign: align || 'center'
            }
        for ( let [ key, val ] of Object.entries(style) ) $text.style[key] = val
        $text.id = 'text_' + id
        $text.innerHTML = text
        this.$el.appendChild($text)
        return $text
    }

    addButton ({ id, text, font, color, fill, stroke, x, y, w, h, b, tag }) {
        const
            $button = document.createElement('button')
          , style = {
                position: 'absolute'
              , boxSizing: 'border-box'
              , font
              , color
              , background: fill
              , border: `${b}px solid ${stroke}`
              , borderBottomWidth: `${b*2}px`
              , marginLeft: x + 'px'
              , marginTop: y + 'px'
              , width: w + 'px'
              , height: h + 'px'
            }
        for ( let [ key, val ] of Object.entries(style) ) $button.style[key] = val
        $button.id = 'button_' + id
        $button.innerHTML = text
        $button.tabIndex = -1 // Undo3D GUI takes over tabbing
        this.$el.appendChild($button)
        return $button
    }

    addInputText ({ id, placeholder, value, font, color, placeholderColor, fill, stroke, x, y, w, h, b, tag }) {
        const
            $inputText = document.createElement('input')
          , style = {
                position: 'absolute'
              , boxSizing: 'border-box'
              , font
              , color
              , background: fill
              , border: `${b}px solid ${stroke}`
              , marginLeft: x + 'px'
              , marginTop: y + 'px'
              , width: w + 'px'
              , height: h + 'px'
            }
        for ( let [ key, val ] of Object.entries(style) ) $inputText.style[key] = val
        $inputText.id = 'input-text_' + id
        $inputText.type = 'text'
        $inputText.setAttribute('placeholder', placeholder)
        if (value) $inputText.value = value
        $inputText.tabIndex = -1 // Undo3D GUI takes over tabbing
        this.$el.appendChild($inputText)
        return $inputText
    }

    addBox ({ id, fill, stroke, x, y, z, w, h, d, b, tag }) {
        const
            $box = document.createElement('div')
          , style = {
                position: 'absolute'
              , boxSizing: 'border-box'
              , background: fill
              , border: `${b}px solid ${stroke}`
              , marginLeft: x + 'px'
              , marginTop: y + 'px'
              , width: w + 'px'
              , height: h + 'px'
            }
        for ( let [ key, val ] of Object.entries(style) ) $box.style[key] = val
        $box.id = 'box_' + id
        this.$el.appendChild($box)
        return $box
    }

    render () {
        // The browser renders automatically
    }

}
