import InputElement from '../../utility/input-element.mjs'

export default class DomInputText extends InputElement {

    constructor (options, frpLib, renderer) {
        super(options, frpLib, renderer)
        this.value = this.value || '' // super has set the value to options.value - but it may be undefined
        const
            { id, placeholder, value, x, y, w } = this
          , h = 40, b = 2
          , font = '18px sans-serif'
        this.$el = renderer.addInputText({
            id, font, fill:'#213', stroke:'#5599F8', placeholder, value
          , color:'#5599F8', placeholderColor:'#3366AA'
          , x, y, w, h, b, tag:'input-text' })
        // this.$box = renderer.addBox({
        //     id, fill:'#213', stroke:'#5599F8', x, y, w, h, b, tag:'input-text' })
        // this.$text = renderer.addText({
        //     id, fill:this.value?'#5599F8':'#3366AA', text:this.value||placeholder
        //   , x:x+10, y:y+12, w:w-10, h:h-12, font, align:'left', tag:'input-text' })
    }

    // setText (options) {
    //     this.$text.innerHTML = options.text
    // }

//// Inherited methods:
///
//  on ()

}
