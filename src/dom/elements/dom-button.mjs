import InputElement from '../../utility/input-element.mjs'

export default class DomButton extends InputElement {

    constructor (options, frpLib, renderer) {
        super(options, frpLib, renderer)
        const
            { id, text, x, y, w } = this
          , h = 40, b = 2
          , font = '18px sans-serif'
        this.$el = renderer.addButton({
            id, text, font, color:'#5599F8', fill:'#213', stroke:'#5599F8'
          , x, y, w, h, b, tag:'button' })
    }

    setText (options) {
        this.$el.innerHTML = options.text
    }

//// Inherited methods:
///
//  on ()

}
