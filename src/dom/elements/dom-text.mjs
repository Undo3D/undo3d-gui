import Element from '../../utility/element.mjs'

export default class DomText extends Element {

    constructor (options, frpLib, renderer) {
        super(options, frpLib, renderer)
        const
            { id, text, x, y, w } = this
          , h = 20
          , font = '18px sans-serif'
        this.$el = renderer.addText({ id, fill:'#5599F8', text, x, y:y+12, w, h, font })
    }

    setText (options) {
        this.$el.innerHTML = options.text
    }

//// Inherited methods:
///
//  on ()

}
