import Gui from '../utility/gui.mjs'
import InputElement from '../utility/input-element.mjs' // used for `instanceof InputElement`
import DomButton from './elements/dom-button.mjs'
import DomInputText from './elements/dom-input-text.mjs'
import DomText from './elements/dom-text.mjs'
import DomRenderer from './dom-renderer.mjs'

export default class DomGui extends Gui {

    //// Override placeholder Gui properties.
    static get Button () { return DomButton }
    static get Text () { return DomText }
    static get InputText () { return DomInputText }
    static get Renderer () { return DomRenderer }

    constructor (options) {
        super(options)

        this._currentFocus = this.current.focus
        Object.defineProperty(this.current, 'focus', {
            get: () => { return this._currentFocus }
          , set: newFocusEl => {
                if (newFocusEl === this._currentFocus) return newFocusEl
                if (this._currentFocus) this._currentFocus.$el.blur()
                if (newFocusEl) newFocusEl.$el.focus()
                return this._currentFocus = newFocusEl
            }
        })

        this._currentActive = this.current.active
        Object.defineProperty(this.current, 'active', {
            get: () => { return this._currentActive }
          , set: newActiveEl => {
                if (newActiveEl === this._currentActive) return newActiveEl
                if (this._currentActive) this._currentActive.$el.classList.remove('active')
                if (newActiveEl) newActiveEl.$el.classList.add('active')
                return this._currentActive = newActiveEl
            }
        })

        const
            { selector } = this
          , $canvas = document.querySelector(selector)
          , passEventToElement = evt => {
                const id = evt.target.id.split('_')[1]
                for (const element of this.elements) { //@TODO z-order
                    if (element.id === id) {
                        if ('mousedown' === evt.type || 'touchstart' === evt.type
                            && element instanceof DomInputText)
                            this.current.focus = element
                        element.fire(evt.type)
                        ////@TODO update this.current.hover
                        break
                    }
                }
            }
        $canvas.addEventListener('mousedown',  passEventToElement)
        $canvas.addEventListener('mouseup',    passEventToElement) //@TODO prevent firing on element when touch is outside its bounds
        $canvas.addEventListener('touchstart', passEventToElement)
        $canvas.addEventListener('touchend',   passEventToElement)
        $canvas.addEventListener('mousemove',  passEventToElement)
        $canvas.addEventListener('mouseover',  passEventToElement)
        $canvas.addEventListener('mouseout',   passEventToElement)
    }


//// Inherited methods:
///
//  addButton (options)
//  addText (options)
//  render ()

}
