export default class Element {

    constructor (options, frpLib, renderer) {
        for ( let [ key, value ] of Object.entries(options) )
            this[key] = value
        this.frpLib = frpLib
        this.renderer = renderer
        this.id = Math.random().toString(36).slice(2) //@TODO better IDs
        this.callbacks = {}
    }

    on (eventNames) { // space-delimited list of event names, eg "click mouseup"
        const { Unit, StreamSink } = this.frpLib
        const streamSink = new StreamSink()
        eventNames.split(' ').forEach( eventName => {
            eventName = eventName.trim()
            this.callbacks[eventName] = this.callbacks[eventName] || []
            this.callbacks[eventName].push( evt => streamSink.send(Unit.UNIT) )
        })
        return streamSink
    }

    fire (eventName) {
        if (this.callbacks[eventName])
            this.callbacks[eventName].map( callback => callback() )
    }
}
