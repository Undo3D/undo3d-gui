import InputElement from './input-element.mjs'

export default class Gui {

    //// Declare Gui properties which should be overridden by sub-classes.
    static get Button () { throw Error('Gui.Button must be overidden!') }
    static get InputText () { throw Error('Gui.InputText must be overidden!') }
    static get Text () { throw Error('Gui.Text must be overidden!') }
    static get Renderer () { throw Error('Gui.Renderer must be overidden!') }

    get guiFocus () { return this._guiFocus }
    set guiFocus (isFocus) {
        if (! this.selector) return this._guiFocus = isFocus // Node.js
        if (isFocus && ! this._guiFocus)
            document.querySelector(this.selector).classList.add('gui-focus')
        else if (! isFocus && this._guiFocus)
            document.querySelector(this.selector).classList.remove('gui-focus')
        return this._guiFocus = isFocus
    }

    constructor (options) { // { selector?, w, h, d, frpLib }
        for ( let [ key, value ] of Object.entries(options) )
            this[key] = value

        this.elements = []

        this.current = { // values are null, or references to elements
            hover: null
          , active: null
          , focus: null
        }

        const { w, h, d, selector, assetPath, current, elements } = this
        this.renderer = new this.constructor.Renderer({
            w, h, d, selector, assetPath, current })

        //// ‘GUI focus’ directs keypresses (eg tab) to the proper Undo3D GUI
        //// instance (where several exist on a single page). Node.js is always
        //// true. Browser GUIs start false, and change to true when clicked.
        this._guiFocus = ! selector

        this.nodeEventHub = {
            listeners: {}
          , addEventListener (type, listener) {
                this.listeners[type] = this.listeners[type] || []
                this.listeners[type].push(listener)
            }
          , dispatchEvent (event) {
                if (! this.listeners[event.type]) return
                this.listeners[event.type].forEach(listener => listener(event))
            }
        }
        const eventHub = selector ? window : this.nodeEventHub

        //// Update the `guiFocus` of every Undo3D GUI in the global context.
        const updateGuiFocus = evt => // contains() is also true if evt.target === $gui
            this.guiFocus = document.querySelector(selector).contains(evt.target)
        eventHub.addEventListener('mousedown',  updateGuiFocus)
        eventHub.addEventListener('touchstart', updateGuiFocus)

        //// Deal with keydown.
        eventHub.addEventListener('keydown', evt => {
            if (! this.guiFocus) return // none of our business

            //// Tab and shift-tab.
            if ('Tab' === evt.key) {
                evt.preventDefault() // eg stop DomGui tabbing natively
                const els = evt.shiftKey ? elements.slice().reverse() : elements
                let toFocus = null
                let foundCurrent = ! current.focus
                for (const el of els)
                    // console.info(el === current.focus, foundCurrent, el.text || el.placeholder);
                    if (el === current.focus)
                        foundCurrent = true
                    else if (foundCurrent)
                        if (el instanceof InputElement) {
                            toFocus = el
                            break
                        }
// console.info(toFocus ? toFocus.text || toFocus.placeholder : 'null');
                if (toFocus)
                    current.focus = toFocus//, console.info('focus', toFocus.text || toFocus.placeholder)
                else
                    current.focus = null
                this.render()
            }

            //// Enter (aka Return).
            if ('Enter' === evt.key && current.focus instanceof InputElement) {
                if (current.active === current.focus) return
                current.active = current.focus
                this.render()
            }

        })


        //// Deal with keyup.
        eventHub.addEventListener('keyup', evt => {
            if (! this.guiFocus) return // none of our business

            //// Enter (aka Return).
            if ('Enter' === evt.key && current.focus instanceof InputElement) {
                if (current.active !== current.focus) return
                current.active = null
                this.render()
            }

        })

    }

    addButton (options) { // { text, textStream?, x, y, z?, w, h?, d?, assetPath? }
        const button = new this.constructor.Button(
            options
          , this.frpLib
          , this.renderer
        )
        this.elements.push(button)
        this.render()
        return button
    }

    addInputText (options) {
        const inputText = new this.constructor.InputText(
            options
          , this.frpLib
          , this.renderer
        )
        this.elements.push(inputText)
        this.render()
        return inputText
    }

    addText (options) { // { text, textStream?, x, y, z?, w, h?, d?, assetPath? }
        const text = new this.constructor.Text(
            options
          , this.frpLib
          , this.renderer
        )
        this.elements.push(text)
        this.render()
        return text
    }

    render () {
        this.renderer.render()
    }

}
