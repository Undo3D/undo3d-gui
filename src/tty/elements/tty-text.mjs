import Element from '../../utility/element.mjs'

export default class TtyText extends Element {

    constructor (options, frpLib, renderer) {
        super(options, frpLib, renderer)
        const
            { id, text, x, y, w } = this
          , th = 1 / renderer.HS // text-height in pixels
          , h = this.h = th
        this.$text = renderer.addText({ fill:'#5599F8', text, x:x+(w/2), y:y+th })
    }

    setText (options) {
        this.$text.text = options.text
        this.renderer.render()
    }

//// Inherited methods:
///
//  on ()

}
