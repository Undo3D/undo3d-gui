import InputElement from '../../utility/input-element.mjs'

export default class TtyButton extends InputElement {

    constructor (options, frpLib, renderer) {
        super(options, frpLib, renderer)
        const
            { id, text, x, y, w } = this
          , th = 1 / renderer.HS // text-height in pixels
          , h = this.h = th * 3
        this.$box = renderer.addBox({
            id, stroke:'#5599F8', x, y, w, h, tag:'button' })
        this.$text = renderer.addText({
            id, fill:'#5599F8', text, x:x+(w/2), y:y+th, tag:'button' })
    }

    setText (options) {
        this.$text.text = options.text
        this.renderer.render()
    }

//// Inherited methods:
///
//  on ()

}
