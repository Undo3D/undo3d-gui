import InputElement from '../../utility/input-element.mjs'

export default class TtyInputText extends InputElement {

    constructor (options, frpLib, renderer) {
        super(options, frpLib, renderer)
        this.value = this.value || '' // super has set the value to options.value - but it may be undefined
        const
            { id, placeholder, x, y, w } = this
          , th = 1 / renderer.HS // text-height in pixels
          , h = this.h = th * 3
        this.$box = renderer.addBox({
            id, stroke:'#5599F8', x, y, w, h, tag:'input-text' })
        this.$text = renderer.addText({
            id, fill:'#5599F8', text:this.value||placeholder, x:x+20, y:y+th
          , align:'left', tag:'input-text' })
    }

    // setText (options) {
    //     this.$text.text = options.text
    //     this.renderer.render()
    // }

//// Inherited methods:
///
//  on ()

}
