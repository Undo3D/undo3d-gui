export default class TtyRenderer {

    constructor (options) { // { w, h, selector }
        for ( let [ key, value ] of Object.entries(options) ) this[key] = value
        const
            { w, h, selector } = this
          , WS = this.WS = 0.1  // width-scale, converts pixels to characters
          , HS = this.HS = 0.05 // height-scale, converts pixels to lines
        this.W = ~~(w*WS), this.H = ~~(h*HS)

        //// Only browsers have an output element. Node.js uses process.stdout.
        this.$el = selector ? document.querySelector(selector) : null

        this.drawables = []

        //// Node.js should clear the Terminal before starting. @TODO copy, and restore at the end?
        if ('object' === typeof process)
            process.stdout.write('\x1B[2J') // \x1B is ‘escape’

        this.clear()
    }

    clear () {
        this.rows = []
        const { W, H, rows } = this
        for (let y=0; y<H; y++) {
            const row = []
            rows.push(row)
            for (let x=0; x<W; x++)
                row.push(' ')
        }
    }

    set ({ char, X, Y }) {
        const
            { W, H, rows } = this
        if (0 > X || W <= X) return
        if (0 > Y || H <= Y) return
        rows[Y][X] = char
    }

    addText ({ id, text, fill, x, y, align, tag }) {
        const $text = { type:'text', id:'text_'+id, fill, text, x, y, align, tag }
        this.drawables.push($text)
        return $text
    }

    addBox ({ id, stroke, x, y, w, h, tag }) {
        const $box = { type:'box', id:'box_'+id, stroke, x, y, w, h, tag }
        this.drawables.push($box)
        return $box
    }

    draw (drawable) {
        if ('text' === drawable.type) return this.drawText(drawable)
        if ('box'  === drawable.type) return this.drawBox(drawable)
    }

    drawText ({ id, text, fill, x, y, align }) {
        const
            { current, WS, HS, rows } = this
          , isActive = (current.active?current.active.id:0)===id.split('_')[1]
          , TW = text.length // text-width in characters
          , X = 'left' === align
              ? ~~(x*WS) //@TODO allow range-right
              : ~~(x*WS - TW/2)
          , Y = ~~(y*HS) + (isActive?1:0)
          , skipSGR = ! fill || 'object' !== typeof process // ‘Select Graphic Rendition’
          // , prefix = skipSGR ? '' : `\x1B[38;2;255;180;0m` // set an RGB color @TODO fix on Terminal.app?
          , prefix = skipSGR ? '' : `\x1B[38;5;33m` // set a 256-mode color
          , suffix = skipSGR ? '' : `\x1B[0m` // reset all attributes
        for (let c=0; c<text.length; c++)
            this.set({ char:text[c], X:X+c, Y })
        rows[Y][X] = prefix + rows[Y][X]
        rows[Y][X+text.length-1] += suffix
    }

    drawBox ({ id, stroke, x, y, w, h, tag }) {
        const
            { current, WS, HS, rows } = this
          , isActive = (current.active?current.active.id:0)===id.split('_')[1]
          , isFocus  = (current.focus ?current.focus.id:0) ===id.split('_')[1]
          , isButton    = 'button' === tag
          , isInputText = 'input-text' === tag
          , TT = false // isActive && isInputText // draw a thick line at the top
          , TB = ! isActive && isButton // draw a thick line at the bottom
          , X = ~~(x*WS), Y = ~~(y*HS) + (isActive?1:0)
          , W = ~~(w*WS), H = ~~(h*HS)

          // , tl = f? T?'╔':'╓': T?'╒':'┌'
          // , tr = f? T?'╗':'╖': T?'╕':'┐'
          // , bl = f? T?'╙':'╚': T?'└':'╘'
          // , br = f? T?'╜':'╝': T?'┘':'╛'
          // , t  = T? '═':'─'
          // , b  = T? '─':'═'
          // , l  = f? '║':'│'
          // , r  = f? '║':'│'

          , tl = isFocus?'╔': TT?'╒':'┌'
          , tr = isFocus?'╗': TT?'╕':'┐'
          , bl = isFocus?'╚': TB?'╘':'└'
          , br = isFocus?'╝': TB?'╛':'┘'
          , t  = isFocus?'═': TT?'═':'─'
          , b  = isFocus?'═': TB?'═':'─'
          // , l  = isFocus?isActive?'╙':'║': isActive?'└':'│'
          // , r  = isFocus?isActive?'╜':'║': isActive?'┘':'│'
          , l  = isFocus? '║':'│'
          , r  = isFocus? '║':'│'

          , skipSGR = ! stroke || 'object' !== typeof process // ‘Select Graphic Rendition’
          // , prefix = skipSGR ? '' : `\x1B[38;2;255;180;0m` // set an RGB color @TODO fix on Terminal.app?
          , prefix = skipSGR ? '' : `\x1B[38;5;33m` // set a 256-mode color
          , suffix = skipSGR ? '' : `\x1B[0m` // reset all attributes
          // , suffix = skipSGR ? '' : `\x1B[38;5;219m` // set a 256-mode color

        this.set({ char:tl, X,       Y })
        this.set({ char:tr, X:X+W-2, Y })
        if (! isActive) this.set({ char:bl, X,       Y:Y+H-1, stroke })
        if (! isActive) this.set({ char:br, X:X+W-2, Y:Y+H-1 })
        this.drawHorizontalLine({ char:t, X:X+1,   Y,       W:W-3 })
        if (! isActive) this.drawHorizontalLine({ char:b, X:X+1,   Y:Y+H-1, W:W-3 })
        this.drawVerticalLine(  { char:l, X,       Y:Y+1,   H:H-2 })
        this.drawVerticalLine(  { char:r, X:X+W-2, Y:Y+1,   H:H-2 })
        for (let y=Y+1; y<Y+H-1; y++)
            this.drawHorizontalLine({ char:' ', X:X+1, Y:y, W:W-3 })

        //// In Node.js, set border color.
        rows[Y][X] = prefix + rows[Y][X] // begin top border
        rows[Y][X+W-2] += suffix // end top border
        if (! isActive) {
            rows[Y+H-1][X] = prefix + rows[Y+H-1][X] // begin bottom border
            rows[Y+H-1][X+W-2] += suffix // end bottom border
        }
        for (let y=Y+1; y<Y+H-1; y++) {
            rows[y][X]     = prefix + rows[y][X]     + suffix // left border
            rows[y][X+W-2] = prefix + rows[y][X+W-2] + suffix // right border
        }
    }

    drawHorizontalLine ({ char, X, Y, W }) {
        for (let x=X; x<X+W; x++)
            this.set({ char, X:x, Y })
    }

    drawVerticalLine ({ char, X, Y, H }) {
        for (let y=Y; y<Y+H; y++)
            this.set({ char, X, Y:y })
    }

    render () {
        this.clear()
        this.drawables.forEach( drawable => this.draw(drawable) )
        const out = this.rows.map( row => row.join('') ).join('\n')
        if (this.$el)
            this.$el.innerHTML = out
        else if ('object' === typeof process)
            process.stdout.write( // \x1B is ‘escape’
                '\x1B[0;0H' // jump to the top-left corner
              + '\x1B[K' // and clear the line
              + '\x1B[1B\x1B[K'.repeat(this.h * this.HS) // clear each line
              + '\x1B[0;0H' // back up to the top-left corner
              + out // and render the drawables
            )
        else
            throw Error('Environment not recognised')
    }

}
