import InputElement from '../utility/input-element.mjs' // used for `instanceof TtyInput`
import TtyButton from './elements/tty-button.mjs' // used for `instanceof TtyButton`

export default class TtyNodeHelper {

    constructor ({ elements, current, renderer, eventHub, WS, HS }) {

        this.elements = elements
        this.current = current
        this.renderer = renderer
        this.eventHub = eventHub
        this.WS = WS
        this.HS = HS


        //// Get chunks on a keystroke-by-keystroke basis.
        //// Begin reading from stdin so the process does not exit.
        //// Expect regular text, not binary.
        process.stdin.setRawMode(true)
        process.stdin.resume()
        process.stdin.setEncoding('utf8')


        //// Reveal the individual bytes of each stdin chunk.
        process.stdin.on('data', raw => {
            const chunk = raw.toString('utf8')
            //
            // //// Move cursor to start of line and clear.
            // process.stdout.write('\x1B[999D\x1B[K') // 1B is ‘escape’

            //// Deal with ctrl-c.
            if ('\u0003' === chunk)
                return process.stdin.pause()

            //// For a mouse event, parse the chunk.
            const md = parseMouseData(chunk)

            ////
            if (md) {
                if (md.mousedown)
                    this.passEventToElement({ type:'mousedown', x:md.x, y:md.y })
                if (md.mouseup)
                    this.passEventToElement({ type:'mouseup', x:md.x, y:md.y })
                if (md.mousemove)
                    this.passEventToElement({ type:'mousemove', x:md.x, y:md.y })
            } else {
                let key = toKey(chunk), shiftKey = false
                if ('Escape' === key) // stdin gives us 'Escape' for shift+tab
                    key = 'Tab', shiftKey = true
                this.eventHub.dispatchEvent({
                    type: 'keydown'
                  , key
                  , shiftKey
                  , preventDefault () {}
                })
                if ('Enter' === key)
                    setTimeout( () => // simulate keyup
                        this.eventHub.dispatchEvent({
                            type: 'keyup'
                          , key: 'Enter'
                          , shiftKey
                          , preventDefault () {}
                        })
                      , 150
                    )
            }

            // if (md)
            //     process.stdout.write(
            //         md.modifier.toString(2).padStart(8, '0')
            //       + ' '
            //       + (md.shift  ? 'shift ' : '')
            //       + (md.meta   ? 'meta ' : '')
            //       + (md.ctrl   ? 'ctrl ' : '')
            //       + (md.scroll ? 'scroll ' : '')
            //       + (md.down   ? 'down ' : '')
            //       + (md.up     ? 'up ' : '')
            //       + (md.move   ? 'move ' : '')
            //       + (md.click  ? 'click ' : '')
            //       + (md.left   ? 'left ' : '')
            //       + (md.middle ? 'middle ' : '')
            //       + (md.right  ? 'right ' : '')
            //       + (md.none   ? 'none ' : '')
            //       + (md.mousedown ? 'mousedown ' : '')
            //       + (md.mouseup   ? 'mouseup ' : '')
            //       + (md.mousemove ? 'mousemove ' : '')
            //       + `(${md.x},${md.y})  `
            //     )

            //
            // // Display the chunk in decimal, hex and ascii, and show its length.
            // if (! md || ! md.scroll)
            //     process.stdout.write(
            //         chunk.split('').map( char => char.charCodeAt(0) ).join(' ')
            //       + '  '
            //       + chunk.split('').map( char => char.charCodeAt(0).toString(16) ).join(' ')
            //       + '  '
            //       + chunk.split('').map(toAscii).join(' ')
            //       + '  (' + chunk.length + ')'
            //     )

        })




        //// LIFECYCLE

        //// Enable mouse-reporting.
        process.stdout.write('\x1B[?1005h') // Enable UTF-8 mouse mode @TODO maybe a different mode if this breaks
        process.stdout.write('\x1B[?1003h') // Use all motion mouse tracking

        //// Disable mouse-reporting after ctrl-c.
        process.on('exit', () => {
            process.stdout.write('\x1B[?1005l')
            process.stdout.write('\x1B[?1003l')
            process.stdout.write('bye!\n')
            process.exit(0)
        })

    }




    passEventToElement (evt) {

        //// Determine which element, if any, should get the event.
        const
            X = evt.x / this.WS
          , Y = evt.y / this.HS
        let target = null
        for (const element of this.elements) { //@TODO z-order
            const { x, y, w, h } = element
            if (x <= X && x+w >= X && y <= Y && y+h >= Y) {
                target = element
                break
            }
        }

        //// Derive 'mouseover' and 'mouseout' from 'mousemove'.
        let mouseoutElement = null
        let mouseoverElement = null
        let mousemoveElement = null
        if ('mousemove' === evt.type) {
            if (null === target && null === this.current.hover) { // continued moving over nothing
                mousemoveElement = null
            } else if (target === this.current.hover) { // moving over current hover-element
                mousemoveElement = target
            } else if (null === target && this.current.hover) { // moved from an element to nothing
                mouseoutElement = this.current.hover
                mousemoveElement = null
            } else if (target && null === this.current.hover) { // moved from nothing to an element
                mouseoverElement = target
                mousemoveElement = target
            } else if (target && this.current.hover) { // moved from one element to another
                mouseoutElement = this.current.hover
                mouseoverElement = target
                mousemoveElement = target
            }
            if (mouseoutElement)  mouseoutElement.fire('mouseout') //@TODO send mouseout when window loses focus, etc
            if (mouseoverElement) mouseoverElement.fire('mouseover')
            if (mousemoveElement) mousemoveElement.fire('mousemove')
            this.current.hover = mousemoveElement // could be null

            //// Deal with events which alter 'active' and 'focus' state.
            } else if (target) {
try {
                if (target instanceof InputElement)
                    if ('mousedown' === evt.type || 'touchstart' === evt.type)
                        this.current.focus = target instanceof TtyButton ? null : target
                      , this.current.active = target
                    else if ('mouseup' === evt.type || 'touchend' === evt.type)
                        this.current.active = null
                target.fire(evt.type)
                this.renderer.render()
} catch (e) { console.log(e);}
            } else { // no target
                if ('mousedown' === evt.type || 'touchstart' === evt.type) {
                    this.current.focus = null
                    this.current.active = null
                    this.renderer.render()
                }
            }



    }

}




//// UTILITY

function toAscii (char) {
    const code = char.charCodeAt(0)
    if (33 <= code && 126 >= code) return char // printable, not space
    if (127 === code) return 'DEL' // delete
    if (127 < code) return 'HI!' //@TODO deal with this better
    return (['NUL','SOH','STX','ETX','EOT','ENQ','ACK','BEL'
            ,'BS', 'HT', 'LF', 'VT', 'FF', 'CR', 'SO', 'SI'
            ,'DLE','DC1','DC2','DC3','DC4','NAK','SYN','ETB'
            ,'CAN','EM', 'SUB','ESC','FS', 'GS', 'RS', 'US'
            ,'SP'])[code]
}

function toKey (char) { //@TODO these should all follow the `key` property from a browser’s key event
    const code = char.charCodeAt(0)
    if (33 <= code && 126 >= code) return char // printable, not space
    if (127 === code) return 'DEL' // delete
    if (127 < code) return 'HI!' //@TODO deal with this better
    return (['NUL','SOH','STX','ETX','EOT','ENQ','ACK','BEL'
            ,'BS', 'Tab', 'LF', 'VT', 'FF', 'Enter', 'SO', 'SI'
            ,'DLE','DC1','DC2','DC3','DC4','NAK','SYN','ETB'
            ,'CAN','EM', 'SUB','Escape','FS', 'GS', 'RS', 'US'
            ,'SP'])[code]
}

//// Based on https://gist.github.com/TooTallNate/1702813
function parseMouseData (chunk) {
    if ( '\u001b[M' !== chunk.slice(0,3) ) return
    const
        x = chunk.charCodeAt(4) - 32 // ascii 33, '!', is 1
      , y = chunk.charCodeAt(5) - 32

      , modifier = chunk.charCodeAt(3)
      , m3  = modifier & 3
      , m96 = modifier & 96
      , m97 = modifier & 97

        //// Modifier keys.
      , shift = !!(modifier & 4)  // bit 4 is on:  .....1..
      , meta  = !!(modifier & 8)  // bit 8 is on:  ....1...
      , ctrl  = !!(modifier & 16) // bit 16 is on: ...1....

        //// Scrolling.
      , scroll = 96 === m96 // bits 64 and 32 are on:    .11.....
      , down   = 97 === m97 // bits 64, 32 and 1 on:     .11....1
      , up     = 96 === m97 // bits 64 and 32 on, 1 off: .11....0

        //// Move, or mouse-button down.
      , move   = 64 === m96 // bit 64 on, 32 off: .10.....
      , click  = 32 === m96 // bit 64 off, 32 on: .01.....

        //// Identify the button.
      , left   = ! scroll && 0 === m3 // not scrolling, 2 and 1 off: .**...00
      , middle = ! scroll && 1 === m3 // not scrolling, 2 off, 1 on: .**...01
      , right  = ! scroll && 2 === m3 // not scrolling, 2 on, 1 off: .**...10
      , none   = ! scroll && 3 === m3 // not scrolling, 2 and 1 on:  .**...11

        //// Dom-like.
      , mousedown = click && ! none
      , mouseup = click && none
      , mousemove = move

    return {
        modifier, x, y
      , shift, meta, ctrl
      , scroll, down, up
      , move, click
      , left, middle, right, none
      , mousedown, mouseup, mousemove
    }
}
