export default class CanvasRenderer {

    constructor (options) { // { w, h, selector }
        for ( let [ key, value ] of Object.entries(options) ) this[key] = value
        const
            { w, h, selector } = this
          , $canvas = this.$canvas = document.querySelector(selector)
          , context = this.context = $canvas.getContext('2d')

        $canvas.width = w
        $canvas.height = h
        $canvas.style.width = w + 'px'
        $canvas.style.height = h + 'px'

        this.drawables = []
    }

    addText ({ id, fill, text, x, y, font, align, tag }) {
        const $text = { type:'text', id:'text_'+id, fill, text, x, y, font, align, tag }
        this.drawables.push($text)
        return $text
    }

    addBox ({ id, fill, stroke, x, y, w, h, b, tag }) {
        const $box = { type:'box', id:'box_'+id, fill, stroke, x, y, w, h, b, tag }
        this.drawables.push($box)
        return $box
    }

    draw (drawable) {
        if ('text' === drawable.type) return this.drawText(drawable)
        if ('box'  === drawable.type) return this.drawBox(drawable)
    }

    drawText ({ id, text, fill, x, y, font, align, tag }) {
        const
            { context, current } = this
          , isActive = (current.active?current.active.id:0)===id.split('_')[1]
          // , isButton    = 'button' === tag
          // , isInputText = 'input-text' === tag
        context.textAlign = align || 'center'
        context.font = font
        context.fillStyle = fill
        if (isActive) {
            context.fillText(text, x, y+2)
        } else {
            context.fillText(text, x, y)
        }
    }

    drawBox ({ id, fill, stroke, x, y, w, h, b, tag }) {
        const
            { context, current } = this
          , isActive = (current.active?current.active.id:0)===id.split('_')[1]
          , isFocus  = (current.focus ?current.focus.id:0) ===id.split('_')[1]
          , isButton    = 'button' === tag
          , isInputText = 'input-text' === tag
        context.fillStyle = fill
        context.strokeStyle = stroke
        context.lineWidth = b
        if (isActive && isButton) {
            context.fillRect(x+b/2, y+b/2, w-b, h-b*2)
            context.strokeRect(x+b/2, y+b/2, w-b, h-b*2)
        } else {
            context.fillRect(x+b/2, y-b/2, w-b, h-b)
            context.strokeRect(x+b/2, y-b/2, w-b, h-b)
        }
        context.beginPath()
        if (isActive && isInputText) { // draw line at top
            context.moveTo(x+b, y+b*0.5)
            context.lineTo(x+w-b, y+b*0.5)
        } else if (! isActive && isButton) { // draw line at bottom
            context.moveTo(x+b, y+h-b*2.5)
            context.lineTo(x+w-b, y+h-b*2.5)
        }
        if (isFocus) {
            // context.moveTo(x-b*1.5, y-b)
            // context.lineTo(x-b*1.5, y-b + h)
            // context.moveTo(x + w + b*1.5, y-b)
            // context.lineTo(x + w + b*1.5, y-b + h)
            if (isActive && isButton)
                context.strokeRect(x-b*1.5, y-b*1.5, w+b*3, h+b*2)
            else
                context.strokeRect(x-b*1.5, y-b*2.5, w+b*3, h+b*3)
        }
        context.stroke()

    }

    render () {
        // console.info(this.context)
        this.context.clearRect(0, 0, this.w, this.h)
        this.drawables.forEach( drawable => this.draw(drawable) )
    }

}
