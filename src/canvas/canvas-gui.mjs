import Gui from '../utility/gui.mjs'
import InputElement from '../utility/input-element.mjs' // used for `instanceof InputElement`
import CanvasButton from './elements/canvas-button.mjs'
import CanvasInputText from './elements/canvas-input-text.mjs'
import CanvasText from './elements/canvas-text.mjs'
import CanvasRenderer from './canvas-renderer.mjs'

export default class CanvasGui extends Gui {

    //// Override placeholder Gui properties.
    static get Button () { return CanvasButton }
    static get InputText () { return CanvasInputText }
    static get Text () { return CanvasText }
    static get Renderer () { return CanvasRenderer }

    constructor (options) {
        super(options)

        const
            { selector } = this
          , $canvas = document.querySelector(selector)
          , passEventToElement = evt => {

                //// Add `x` and `y` to the event object, if this is a touch.
                const touches = evt.changedTouches || evt.targetTouches || null
                if (touches)
                    if (0 === touches.length)
                        return
                    else
                        evt.x = touches[0].clientX //@TODO deal with multiple touches
                      , evt.y = touches[0].clientY //@TODO deal with multiple touches

                //// Determine which element, if any, should get the event.
                const
                    { x, y } = $canvas.getBoundingClientRect()
                  , X = evt.x - x
                  , Y = evt.y - y
                let target = null
                for (const element of this.elements) { //@TODO z-order
                    const { x, y, w, h } = element
                    if (x <= X && x+w >= X && y <= Y && y+h >= Y) {
                        target = element
                        break
                    }
                }

                //// Derive 'mouseover' and 'mouseout' from 'mousemove'.
                let mouseoutElement = null
                let mouseoverElement = null
                let mousemoveElement = null
                if ('mousemove' === evt.type) {
                    if (null === target && null === this.current.hover) { // continued moving over nothing
                        mousemoveElement = null
                    } else if (target === this.current.hover) { // moving over current hover-element
                        mousemoveElement = target
                    } else if (null === target && this.current.hover) { // moved from an element to nothing
                        mouseoutElement = this.current.hover
                        mousemoveElement = null
                    } else if (target && null === this.current.hover) { // moved from nothing to an element
                        mouseoverElement = target
                        mousemoveElement = target
                    } else if (target && this.current.hover) { // moved from one element to another
                        mouseoutElement = this.current.hover
                        mouseoverElement = target
                        mousemoveElement = target
                    }
                    if (mouseoutElement)  mouseoutElement.fire('mouseout')
                    if (mouseoverElement) mouseoverElement.fire('mouseover')
                    if (mousemoveElement) mousemoveElement.fire('mousemove')
                    this.current.hover = mousemoveElement // could be null

                //// Deal with events which alter 'active' and 'focus' state.
                } else if (target) {
                    if (target instanceof InputElement)
                        if ('mousedown' === evt.type || 'touchstart' === evt.type)
                            this.current.focus = target instanceof CanvasButton ? null : target
                          , this.current.active = target
                        else if ('mouseup' === evt.type || 'touchend' === evt.type)
                            this.current.active = null
                    target.fire(evt.type)
                    this.renderer.render()

                } else { // no target
                    if ('mousedown' === evt.type || 'touchstart' === evt.type) {
                        this.current.focus = null
                        this.current.active = null
                        this.renderer.render()
                    }
                }

            }
        $canvas.addEventListener('mousedown',  passEventToElement)
        $canvas.addEventListener('mouseup',    passEventToElement)
        $canvas.addEventListener('touchstart', passEventToElement)
        $canvas.addEventListener('touchend',   passEventToElement)
        $canvas.addEventListener('mousemove',  passEventToElement)

        //// End hover if the user switches window focus.
        window.addEventListener('blur', evt => {
            if (null === this.current.hover) return
            this.current.hover.fire('mouseout')
            this.current.hover = null
        })

    }

//// Inherited methods:
///
//  addButton (options)
//  addInputText (options)
//  addText (options)
//  render ()

}
