import InputElement from '../../utility/input-element.mjs'

export default class CanvasButton extends InputElement {

    constructor (options, frpLib, renderer) {
        super(options, frpLib, renderer)
        const
            { id, text, x, y, w } = this
          , b = 2 // border width
          , h = this.h = 40
          , font = '18px sans-serif'
        this.$box = renderer.addBox({
            id, fill:'#213', stroke:'#5599F8', x, y, w, h, b, tag:'button' })
        this.$text = renderer.addText({
            id, fill:'#5599F8', text, x:x+(w/2), y:y+24, font, tag:'button' })
        renderer.render()
    }

    setText (options) {
        this.$text.text = options.text
        this.renderer.render()
    }

//// Inherited methods:
///
//  on ()

}
