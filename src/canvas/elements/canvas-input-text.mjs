import InputElement from '../../utility/input-element.mjs'

export default class CanvasInputText extends InputElement {

    constructor (options, frpLib, renderer) {
        super(options, frpLib, renderer)
        this.value = this.value || '' // super has set the value to options.value - but it may be undefined
        const
            { id, placeholder, x, y, w } = this
          , b = 2 // border width
          , h = this.h = 40
          , font = '18px sans-serif'
        this.$box = renderer.addBox({
            id, fill:'#213', stroke:'#5599F8', x, y, w, h, b, tag:'input-text' })
        this.$text = renderer.addText({
            id, fill:this.value?'#5599F8':'#3366AA', text:this.value||placeholder, x:x+10, y:y+24, font, align:'left', tag:'input-text' })
        renderer.render()
    }

    // setText (options) {
    //     this.$text.text = options.text
    //     this.renderer.render()
    // }

//// Inherited methods:
///
//  on ()

}
