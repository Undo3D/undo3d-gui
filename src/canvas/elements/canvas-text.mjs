import Element from '../../utility/element.mjs'

export default class CanvasText extends Element {

    constructor (options, frpLib, renderer) {
        super(options, frpLib, renderer)
        const
            { id, text, x, y, w } = this
          , font = '18px sans-serif'
        this.$text = renderer.addText({ id, fill:'#5599F8', text, x:x+(w/2), y:y+24, font })
        renderer.render()
    }

    setText (options) {
        this.$text.text = options.text
        this.renderer.render()
    }

//// Inherited methods:
///
//  on ()

}
